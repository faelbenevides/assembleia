# Projeto Assembleia

Projeto destinado para apresentação do teste de conhecimentos, seguindo a recomendação do documento `Desafio Técnico Back-End - TIME PIX.pdf`.

## Contextualizando a escolha da stack do projeto

O Spring Boot (versão 2.5.0) foi escolhido como stack para a solução por ser um framework que dispõe de diversos recursos e ferramentas, necessárias tanto ao desafio principal quanto as tarefas bônus.

Embora o projeto seja simplório, foi necessário uso de integrações com outras ferramentas para realizar as tarefas bônus, implicando mais dependências e configurações.

Dentre as principais ferramentas podemos destacar: Liquibase para simplificar a criação e versionamento da estrutura do banco, Apache Kafka para o envio de mensagens, Docker para iniciar os serviços auxiliares, Lombok para deixar o codigo limpo e melhorar manutenção, Mapstruct como mapeamento entre dto's e entidades, SpringFox para documentação da API REST, dentre outras ferramentas para executar testes e criar documentação.

As tecnicas utilizadas aqui visam seguir todos os itens descritos na sessão: `O que será analisado`

A partir daqui, a documentação sintetizará como configurar todo o ambiente para que a aplicação possa ser executada.

## Desenvolvimento

No perfil `dev` usamos o banco `H2 Database` para a persistência de dados, não sendo necessário alterar qualquer configuração, porém é necessário a disponibilização do serviço de gerenciamento de mensagens Apache Kafka e o servidor Apache ZooKeeper, ver como sugestão a sessão `Usando Docker...`

Para iniciar seu aplicativo no perfil `dev`, execute:

```
./mvnw
```

## Acessando o console H2 Database

O banco de dados H2 possui um console GUI embutido para navegar pelo conteúdo de um banco de dados e executar consultas SQL. Por padrão, o console H2 não é habilitado no Spring.

Para habilitá-lo, precisamos adicionar a seguinte propriedade em `application-dev.yml`:

```properties
spring:
  h2:
    console:
      enabled: true
```

Então, depois de iniciar o aplicativo, podemos navegar para http://localhost:8081/h2-console, que nos apresentará uma página de login.

Na página de login, forneceremos as mesmas credenciais que usamos em `application-dev.yml`:

![H2 Console Login](doc/md/h2-print-01.png)

Assim que nos conectarmos, veremos uma página da web abrangente que lista todas as tabelas no lado esquerdo da página e uma caixa de texto para a execução de consultas SQL:

![H2 Console Dashboard](doc/md/h2-print-02.png)

## Produção

No perfil `prod` usamos o banco `PostgreSQL` para a persistência de dados, é necessário a disponibilização do serviço do banco PostgreSQL, gerenciamento de mensagens Apache Kafka e o servidor Apache ZooKeeper, ver como sugestão a sessão `Usando Docker...`

### Empacotando como jar

Para construir o jar final e otimizar o aplicativo assembleiaApplication para produção, execute:

```
./mvnw -Pprod clean verify
```

Para garantir que tudo funcionou, execute:

```
java -jar target / *. jar
```

### Empacotando como war

Para empacotar seu aplicativo como um war a fim de implantá-lo em um servidor de aplicativos, execute:

```
./mvnw -Pprod,war clean check
```

## Testando

Para iniciar os testes do seu aplicativo, execute:

```
./mvnw verify
```

### Teste de unidade

O `Maven Surefire Plugin` é usado durante a fase de teste do ciclo de vida da construção para executar os testes de unidade de um aplicativo.

Por padrão, o relatório é gerado em: `${basedir}/target/surefire-reports/`

### Teste de integração

O `Maven Failsafe Plugin` foi projetado para executar testes de integração, enquanto o plug-in Surefire foi projetado para executar testes de unidade.

Por padrão, o relatório é gerado em: `${basedir}/target/failsafe-reports/`

### Teste de arquitetura

O `ArchUnit` é uma biblioteca gratuita, simples e extensível para verificar a arquitetura de seu código Java. Ou seja, o ArchUnit pode verificar dependências entre pacotes e classes, camadas e fatias, verificar dependências cíclicas e muito mais.

### Teste de desempenho

O `Gatling` permite executar testes de desempenho escritos em Scala. Os arquivos de simulação estão localizados em `src/test/gatling/user-files/simulations`.

Para iniciar os testes de desempenho, execute:

```
./mvnw gatling:test
```

Por padrão, o relatório é gerado em: `${basedir}/target/gatling/restsimulation-*`

## Usando Docker para simplificar o desenvolvimento (opcional)

O projeto foi testado na versão 3.3.3(64133)

Você pode usar o Docker para melhorar sua experiência de desenvolvimento. Diversas configurações docker-compose estão disponíveis na pasta [src/main/docker](src/main/docker) para iniciar os serviços de terceiros necessários.

Para iniciar o gerenciador de mensagens Apache Kafka integrado ao servidor Apache ZooKeeper, execute:

```
docker-compose -f src/main/docker/kafka.yml up -d
```

Para interrompê-lo e remover o contêiner, execute:

```
docker-compose -f src/main/docker/kafka.yml down
```

Para iniciar um banco de dados PostgreSQL em um contêiner docker, execute:

```
docker-compose -f src/main/docker/postgresql.yml up -d
```

Para interrompê-lo e remover o contêiner, execute:

```
docker-compose -f src/main/docker/postgresql.yml down
```

Você também pode encaixar totalmente seu aplicativo e todos os serviços dos quais ele depende.
Para conseguir isso, primeiro crie uma imagem docker do seu aplicativo executando:

```
./mvnw -Pprod verify jib:dockerBuild
```

Para executar:

```
docker-compose -f src/main/docker/app.yml up -d
```

Para interrompê-lo e remover o contêiner, execute:

```
docker-compose -f src/main/docker/app.yml down
```

## Documentação

### Documentação da Aplicação

O `Apache Maven Javadoc Plugin` usa a ferramenta Javadoc para gerar javadocs para o projeto especificado. Para obter mais informações sobre a ferramenta Javadoc padrão, consulte o [Guia de Referência](http://docs.oracle.com/javase/7/docs/technotes/tools/windows/javadoc.html).

Para criar a documentação, execute:

```
./mvnw javadoc:javadoc
```

Por padrão, a documentação é gerada em: `${basedir}/target/site/`

### Documentação da API REST

O `Springfox Boot Starter` é o conjunto de bibliotecas java tem como objetivo automatizar a geração de especificações legíveis por máquinas e humanos para APIs JSON escritas usando a família de projetos Spring.

Para ter acesso a documentação da API REST, execute o projeto e acesso o endereço pelo navegador: http://localhost:8081/swagger-ui/