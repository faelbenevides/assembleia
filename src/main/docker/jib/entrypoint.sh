#!/bin/sh

echo "O aplicativo vai começar em ${START_SLEEP}s..." && sleep ${START_SLEEP}
exec java ${JAVA_OPTS} -noverify -XX:+AlwaysPreTouch -Djava.security.egd=file:/dev/./urandom -cp /app/resources/:/app/classes/:/app/libs/* "com.zallpy.assembleia.AssembleiaApplication"  "$@"
