package com.zallpy.assembleia.domain;

/**
 * Enum da situacao da sessão da pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
public enum SessaoSituacao {

    ABERTA, ENCERRADA;

}
