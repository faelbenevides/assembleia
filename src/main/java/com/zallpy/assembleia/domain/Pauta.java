package com.zallpy.assembleia.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entidade da tabela pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "pauta")
public class Pauta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pautaSequenceGenerator")
    @SequenceGenerator(name = "pautaSequenceGenerator", sequenceName = "pauta_sequence", allocationSize = 1)
    @EqualsAndHashCode.Include
    private Long id;

    @Embedded
    @AttributeOverride(name = "inicio", column = @Column(name = "sessao_inicio"))
    @AttributeOverride(name = "fim", column = @Column(name = "sessao_fim"))
    @AttributeOverride(name = "situacao", column = @Column(name = "sessao_situacao"))
    private Sessao sessao;

    @OneToMany(mappedBy = "pauta", cascade = CascadeType.ALL)
    private Set<Voto> votos = new HashSet<>(0);

    /**
     * Adicionar voto na pauta.
     * 
     * @param voto o voto
     */
    public void addVoto(Voto voto) {
        getVotos().add(voto);
    }

}
