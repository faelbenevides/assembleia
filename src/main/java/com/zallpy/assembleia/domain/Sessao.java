package com.zallpy.assembleia.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Propriedades embutidas da sessao da pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Sessao implements Serializable {

    public static final long DURACAO_PADRAO = 60l;

    private Instant inicio;

    private Instant fim;

    @Enumerated(EnumType.STRING)
    private SessaoSituacao situacao;

}
