package com.zallpy.assembleia.service.mapper;

import java.util.List;

import com.zallpy.assembleia.domain.Pauta;
import com.zallpy.assembleia.service.dto.PautaDTO;

import org.mapstruct.Mapper;

/**
 * Mapeamento para entidade pauta.
 */
@Mapper(componentModel = "spring")
public interface PautaMapper {

    PautaDTO toDTO(Pauta entity);

    List<PautaDTO> toDTO(List<Pauta> entity);

}
