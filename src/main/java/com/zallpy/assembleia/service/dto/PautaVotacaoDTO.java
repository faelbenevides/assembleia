package com.zallpy.assembleia.service.dto;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * DTO da contabilização dos votos de uma pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Builder
@Data
public class PautaVotacaoDTO implements Serializable {

    private Long id;
    private int votosSim;
    private int votosNao;

}
