package com.zallpy.assembleia.service.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * DTO da da pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Data
public class PautaDTO implements Serializable {

    private Long id;
    private SessaoDTO sessao;

}
