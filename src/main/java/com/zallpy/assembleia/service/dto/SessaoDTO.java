package com.zallpy.assembleia.service.dto;

import java.io.Serializable;
import java.time.Instant;

import com.zallpy.assembleia.domain.SessaoSituacao;

import lombok.Data;

/**
 * DTO da da sessao.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Data
public class SessaoDTO implements Serializable {

    private Instant inicio;
    private Instant fim;
    private SessaoSituacao situacao;
    
}
