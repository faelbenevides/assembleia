package com.zallpy.assembleia.service.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * DTO do voto do associado.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Data
public class VotoAssociadoDTO {

    @NotNull
    private Long pautaId;
    @NotNull
    private String cpf;
    private boolean favoravel;

}
