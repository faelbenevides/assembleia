package com.zallpy.assembleia.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import com.zallpy.assembleia.client.CpfServiceClient;
import com.zallpy.assembleia.client.dto.ConsultaCpfDTO;
import com.zallpy.assembleia.config.KafkaConfig;
import com.zallpy.assembleia.domain.Pauta;
import com.zallpy.assembleia.domain.Sessao;
import com.zallpy.assembleia.domain.SessaoSituacao;
import com.zallpy.assembleia.domain.Voto;
import com.zallpy.assembleia.exception.BusinessConstraintException;
import com.zallpy.assembleia.repository.PautaRepository;
import com.zallpy.assembleia.service.dto.PautaDTO;
import com.zallpy.assembleia.service.dto.PautaVotacaoDTO;
import com.zallpy.assembleia.service.dto.VotoAssociadoDTO;
import com.zallpy.assembleia.service.mapper.PautaMapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.client.RestClientException;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * Serviço da pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class PautaService {

    private final PautaRepository pautaRepository;
    private final PautaMapper pautaMapper;
    private final CpfServiceClient cpfServiceClient;
    private final TransactionTemplate transactionTemplate;
    private final KafkaTemplate<String, PautaVotacaoDTO> kafkaTemplate;

    @Qualifier("jdbcScheduler")
    private final Scheduler jdbcScheduler;

    /**
     * Obter todos os registros.
     * 
     * @return todas as pautas
     */
    public List<PautaDTO> obterTodos() {
        log.info("Obtendo todas as pautas");
        return pautaMapper.toDTO(pautaRepository.findAll());
    }

    /**
     * Criar uma nova pauta.
     * 
     * @return pauta criada
     */
    public PautaDTO criarPauta() {
        log.info("Criando nova pauta");
        Pauta pauta = new Pauta();
        pauta = pautaRepository.save(pauta);
        return pautaMapper.toDTO(pauta);
    }

    /**
     * Abrir a sessão da pauta.
     * 
     * @param pautaId identificador da pauta
     * @param duracao tempo de duracao da sessao
     * @return pauta com sessão aberta
     * @throws NotFoundException exceção por não localizar registro
     */
    public PautaDTO abrirSessao(Long pautaId, long duracao) throws NotFoundException {
        log.info("Abrindo sessao de {} segundos para a pauta {}", duracao, pautaId);

        // Busca a pauta
        Pauta pauta = findById(pautaId);

        // Cria uma nova sessão ABERTA
        Instant inicio = Instant.now();
        Instant fim = inicio.plusSeconds(duracao);
        Sessao sessao = new Sessao(inicio, fim, SessaoSituacao.ABERTA);

        // Atribui a nova sessão e atualiza a pauta
        pauta.setSessao(sessao);
        pauta = pautaRepository.save(pauta);
        return pautaMapper.toDTO(pauta);
    }

    /**
     * Encerrar a sessão da pauta.
     * 
     * @param pautaId identificador da pauta
     * @return pauta com sessão encerrada
     * @throws NotFoundException exceção por não localizar registro
     */
    public PautaDTO encerrarSessao(Long pautaId) throws NotFoundException {
        // Busca a pauta
        Pauta pauta = findById(pautaId);

        // Encerra a sessão e atualiza a pauta
        pauta = encerrarSessao(pauta);
        return pautaMapper.toDTO(pauta);
    }

    /**
     * Encerrar todoas as sessões que expiraram.
     */
    public void encerrarSessaoExpirada() {
        Instant now = Instant.now();
        pautaRepository.findBySessaoAberta().stream().filter(pauta -> pauta.getSessao().getFim().isBefore(now))
                .forEach(this::encerrarSessao);
    }

    /**
     * Incluir voto na pauta.
     * 
     * @param votoAssociado dto do voto do associado
     * @throws NotFoundException           exceção por não localizar registro
     * @throws BusinessConstraintException exceção por violar regra de negócio
     */
    public void incluirVoto(VotoAssociadoDTO votoAssociado) throws NotFoundException, BusinessConstraintException {
        log.info("Incluindo voto na pauta {} do associado {}", votoAssociado.getPautaId(), votoAssociado.getCpf());

        // Busca a pauta
        Pauta pauta = findById(votoAssociado.getPautaId());

        // Verifica se a pauta não possui sessão ABERTA
        if (pauta.getSessao() == null || !SessaoSituacao.ABERTA.equals(pauta.getSessao().getSituacao()))
            throw new BusinessConstraintException("Pauta não possui sessao ABERTA");

        // Verifica se existe o voto para o associado
        if (pauta.getVotos().stream().anyMatch(voto -> voto.getCpf().equals(votoAssociado.getCpf())))
            throw new BusinessConstraintException("Associado já votou nessa pauta");

        // Verifica se o associado está autorizado a votar
        if (!autorizadoVotar(votoAssociado.getCpf()))
            throw new BusinessConstraintException("Associado não está autorizado a votar");

        // Inclui o voto do associado e atualiza a pauta
        Voto voto = new Voto(null, pauta, votoAssociado.getCpf(), votoAssociado.isFavoravel());
        pauta.addVoto(voto);
        pautaRepository.save(pauta);
    }

    /**
     * Contabilizar os votos da pauta.
     * 
     * @param pautaId identificador da pauta
     * @return votos contabilizados
     * @throws NotFoundException exceção por não localizar registro
     */
    public PautaVotacaoDTO totalVotos(Long pautaId) throws NotFoundException {
        // Busca a pauta
        Pauta pauta = findById(pautaId);

        // Retorna o total da votacao
        return totalVotos(pauta);
    }

    /**
     * Contabilizar os votos da pauta.
     * 
     * @param pauta a pauta
     * @return votos contabilizados
     */
    public PautaVotacaoDTO totalVotos(Pauta pauta) {
        // Total dos votos
        int votosSim = countVotos(pauta.getVotos(), true);
        int votosNao = countVotos(pauta.getVotos(), false);

        // Retorna o total da votacao
        return PautaVotacaoDTO.builder().id(pauta.getId()).votosSim(votosSim).votosNao(votosNao).build();
    }

    /**
     * Obter todos os registros (processo não blocante).
     * 
     * @return todas as pautas
     */
    public Flux<PautaDTO> obterTodosRx() {
        Supplier<Flux<PautaDTO>> supplier = () -> Flux.fromIterable(obterTodos());
        return Flux.defer(supplier).subscribeOn(jdbcScheduler);
    }

    /**
     * Criar uma nova pauta (processo não blocante).
     * 
     * @return pauta criada
     */
    public Mono<PautaDTO> criarPautaRx() {
        TransactionCallback<PautaDTO> action = status -> criarPauta();
        Callable<PautaDTO> supplier = () -> transactionTemplate.execute(action);
        return Mono.fromCallable(supplier).subscribeOn(jdbcScheduler);
    }

    /**
     * Busca a pauta pelo id.
     * 
     * @param pautaId id da pauta
     * @return a pauta
     * @throws NotFoundException exceção por não localizar registro
     */
    private Pauta findById(Long pautaId) throws NotFoundException {
        return pautaRepository.findById(pautaId).orElseThrow(() -> new NotFoundException("Pauta não encontrada"));
    }

    /**
     * Encerra a sessão, atualiza a pauta e posta uma mensagem com o resoltado da
     * votação.
     * 
     * @param pauta a pauta
     * @return a pauta encerrada
     */
    private Pauta encerrarSessao(Pauta pauta) {
        log.info("Encerrando sessao da pauta {}", pauta.getId());

        // Encerra a sessão e atualiza a pauta
        pauta.getSessao().setSituacao(SessaoSituacao.ENCERRADA);
        pauta = pautaRepository.save(pauta);

        // Posta uma mensagem com o resultado da votação
        PautaVotacaoDTO totalVotos = totalVotos(pauta);
        send(totalVotos);

        return pauta;
    }

    /**
     * Contabilizar os votos favoráveis ou não.
     * 
     * @param votosStream stream de votos
     * @param favoravel   se favorável
     * @return quantidade de votos
     */
    private int countVotos(Set<Voto> votos, boolean favoravel) {
        return (int) votos.stream().filter(voto -> voto.isFavoravel() == favoravel).count();
    }

    /**
     * Verifica se o cpf está autorizado para votar, chamada ao client
     * {@link CpfServiceClient}.
     * 
     * @param cpf o cpf do associado
     * @return se está autorizado a votar
     */
    private boolean autorizadoVotar(String cpf) {
        try {
            ResponseEntity<ConsultaCpfDTO> response = cpfServiceClient.verificaCpf(cpf);
            return response.getStatusCode().is2xxSuccessful() && Optional.ofNullable(response.getBody())
                    .map(ConsultaCpfDTO::getStatus).map(status -> status.equals("ABLE_TO_VOTE")).orElse(false);
        } catch (RestClientException e) {
            log.error("Falha na requisição para verificar o CPF", e);
            return false;
        }
    }

    /**
     * Posta uma mensagem com o resultado da votação.
     * 
     * @param data total da votação
     */
    private void send(PautaVotacaoDTO data) {
        log.info("Postando mensagem com resultado da votação da pauta: {}", data.getId());
        kafkaTemplate.send(KafkaConfig.TOTAL_VOTACAO_TOPIC, data).addCallback(this::successCallback,
                this::failureCallback);
    }

    /**
     * Trata o resultado sucesso da postagem.
     * 
     * @param result retorno do envio
     */
    private void successCallback(SendResult<String, PautaVotacaoDTO> result) {
        log.info("Mensagem postada com offset=[{}]", result.getRecordMetadata().offset());
    }

    /**
     * Trata o resultado falha da postagem.
     * 
     * @param ex falha do envio
     */
    private void failureCallback(Throwable ex) {
        log.error("Não foi possível postar a mensagem devido a=[{}]", ex.getMessage());
    }

}
