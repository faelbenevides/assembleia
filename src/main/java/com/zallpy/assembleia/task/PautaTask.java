package com.zallpy.assembleia.task;

import com.zallpy.assembleia.service.PautaService;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * Tarefa para realizar operações agendadas para a pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class PautaTask {

    private final PautaService pautaService;

    /**
     * Busca as pautas com sessao em aberto e encerra as que expiraram.
     */
    @Scheduled(fixedRate = 1000)
    public void reportCurrentTime() {
        pautaService.encerrarSessaoExpirada();
    }

}
