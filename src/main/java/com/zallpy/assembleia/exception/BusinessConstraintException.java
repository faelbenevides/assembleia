package com.zallpy.assembleia.exception;

/**
 * Exceção lançada por violar regra de negócio da aplicação.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
public class BusinessConstraintException extends Exception {

    /**
     * Construtor.
     */
    public BusinessConstraintException() {
        super();
    }

    /**
     * Construtor.
     * 
     * @param message a mensagem da exceção
     */
    public BusinessConstraintException(String message) {
        super(message);
    }

}
