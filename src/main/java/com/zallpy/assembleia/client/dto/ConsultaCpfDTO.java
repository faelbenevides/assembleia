package com.zallpy.assembleia.client.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * DTO que representa o retorno da consulta do CPF.
 */
@Data
public class ConsultaCpfDTO implements Serializable {

    private String status;

}
