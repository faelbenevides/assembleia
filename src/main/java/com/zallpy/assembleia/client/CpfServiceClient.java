package com.zallpy.assembleia.client;

import com.zallpy.assembleia.client.dto.ConsultaCpfDTO;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * Cliente responsável por realizar requisição (blocante e não blocante) ao
 * serviço de verificação de CPF.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Slf4j
@Component
public class CpfServiceClient {

    private static final String CPF_SERVICE_URL = "https://user-info.herokuapp.com/";
    private static final String CPF_ENDPOINT_PATH = "users/{cpf}";

    private final RestTemplate cpfRestTemplate;
    private final WebClient cpfWebClient;

    /**
     * Construtor.
     */
    public CpfServiceClient() {
        log.info("Criando RestTemplate e WebClient para requisições ao serviço de verificação de CPF");
        this.cpfRestTemplate = new RestTemplate();
        this.cpfWebClient = WebClient.create(CPF_SERVICE_URL);
    }

    /**
     * Faz uma requisição (blocante) ao serviço de verificação de CPF e retorna se o
     * CPF está habilitado para votar.
     * 
     * @param cpf o cpf do associado
     * @return resposta da requisição
     * @throws RestClientException exceção da requisição
     */
    public ResponseEntity<ConsultaCpfDTO> verificaCpf(String cpf) throws RestClientException {
        String url = CPF_SERVICE_URL.concat(CPF_ENDPOINT_PATH);
        return cpfRestTemplate.getForEntity(url, ConsultaCpfDTO.class, cpf);
    }

    /**
     * Faz uma requisição (não blocante) ao serviço de verificação de CPF e retorna
     * se o CPF está habilitado para votar.
     * 
     * @param cpf o cpf do associado
     * @return resposta da requisição
     * @throws WebClientResponseException exceção da requisição
     */
    public Mono<ConsultaCpfDTO> verificaCpfRx(String cpf) throws WebClientResponseException {
        String uri = CPF_SERVICE_URL.concat(CPF_ENDPOINT_PATH);
        return cpfWebClient.get().uri(uri, cpf).accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToMono(ConsultaCpfDTO.class);
    }

}
