package com.zallpy.assembleia.config;

import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import lombok.extern.slf4j.Slf4j;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * Configuração de processos não blocantes reativos.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Slf4j
@Configuration
public class ReactorConfig {

    @Value("${spring.datasource.hikari.maximum-pool-size}")
    private int connectionPoolSize;

    @Bean
    public Scheduler jdbcScheduler() {
        log.debug("Criação de executor de processos não blocantes");
        return Schedulers.fromExecutor(Executors.newFixedThreadPool(connectionPoolSize));
    }

	@Bean
    public TransactionTemplate transactionTemplate(PlatformTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }

}
