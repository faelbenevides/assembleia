package com.zallpy.assembleia.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.zallpy.assembleia.service.dto.PautaVotacaoDTO;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Configuração de envio de mensagens para topicos usando Apache Kafka.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "kafka")
public class KafkaConfig {

    public static final String TOTAL_VOTACAO_TOPIC = "totalVotacaoTopic";

    @Setter
    private String bootstrapServers;

    /**
     * Configurações do tópico de mensagens Kafka.
     */
    @Configuration
    public class KafkaTopicConfig {

        @Bean
        public KafkaAdmin kafkaAdmin() {
            Map<String, Object> configs = Collections.singletonMap(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG,
                    bootstrapServers);
            log.info("Configurando kafka client no bootstrap server: {}", bootstrapServers);
            return new KafkaAdmin(configs);
        }

        @Bean
        public NewTopic totalVotacaoTopic() {
            log.info("Criando tópico [{}] no kafka", TOTAL_VOTACAO_TOPIC);
            return new NewTopic(TOTAL_VOTACAO_TOPIC, 1, (short) 1);
        }
    }

    /**
     * Configurações do fornecedor de mensagens Kafka.
     */
    @Configuration
    public class KafkaProducerConfig {

        @Bean
        public ProducerFactory<String, PautaVotacaoDTO> producerFactory() {
            Map<String, Object> configProps = new HashMap<>();
            configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
            configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
            configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
            return new DefaultKafkaProducerFactory<>(configProps);
        }

        @Bean
        public KafkaTemplate<String, PautaVotacaoDTO> kafkaTemplate() {
            return new KafkaTemplate<>(producerFactory());
        }
    }

}
