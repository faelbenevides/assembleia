package com.zallpy.assembleia.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Configuração da documentação da API do Swagger.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Configuration
public class SpringFoxConfig {
                              
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
          .select()
          .apis(RequestHandlerSelectors.any())
          .paths(PathSelectors.any())
          .build()
          .apiInfo(apiInfo());
    }
    
    private ApiInfo apiInfo() {
        Contact contact = new Contact(
            "Rafael Benevides",
            "",
            "faelbenevides@gmail.com");
        return new ApiInfo(
            "Assembleia Api",
            "Documentação da Api REST",
            "0.0.1",
            "",
            contact,
            "",
            "",
            new ArrayList<>());
    }
}
