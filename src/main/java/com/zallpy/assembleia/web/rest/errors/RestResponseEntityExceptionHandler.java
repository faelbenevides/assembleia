package com.zallpy.assembleia.web.rest.errors;

import com.zallpy.assembleia.exception.BusinessConstraintException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javassist.NotFoundException;

/**
 * Tratamento de exceção para a API Rest.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Trata as exceções de violação de regra de negócio.
     * 
     * @param ex      a exceção
     * @param request a requisicao
     * @return tratamento da excecao
     */
    @ExceptionHandler(value = BusinessConstraintException.class)
    protected ResponseEntity<Object> handleBusiness(Exception ex, WebRequest request) {
        String bodyOfResponse = "Violação de regra da aplicação: " + ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    /**
     * Trata as exceções para registro não encontrado.
     * 
     * @param ex      a exceção
     * @param request a requisicao
     * @return tratamento da excecao
     */
    @ExceptionHandler(value = NotFoundException.class)
    protected ResponseEntity<Object> handleNotFound(Exception ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    /**
     * Trata as exceções que foram disparadas por operação ilegal do sistema.
     * 
     * @param ex      a exceção
     * @param request a requisicao
     * @return tratamento da excecao
     */
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Ocorreu uma operação ilegal: " + ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

}
