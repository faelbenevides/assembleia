/**
 * Erros específicos usados para lidar com exceções internas do Spring MVC.
 */
package com.zallpy.assembleia.web.rest.errors;
