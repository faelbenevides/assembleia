package com.zallpy.assembleia.web.rest;

import java.util.List;

import javax.validation.Valid;

import com.zallpy.assembleia.domain.Sessao;
import com.zallpy.assembleia.exception.BusinessConstraintException;
import com.zallpy.assembleia.service.PautaService;
import com.zallpy.assembleia.service.dto.PautaDTO;
import com.zallpy.assembleia.service.dto.PautaVotacaoDTO;
import com.zallpy.assembleia.service.dto.VotoAssociadoDTO;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;

/**
 * Recurso para pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Api(value = "Recurso da pauta")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pauta")
public class PautaResource {

    private final PautaService pautaService;

    @ApiOperation(value = "Obtem todos os registros da pauta")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Retorna a lista de pautas"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @GetMapping
    public ResponseEntity<List<PautaDTO>> obterTodos() {
        List<PautaDTO> pautas = pautaService.obterTodos();
        return ResponseEntity.ok(pautas);
    }

    @ApiOperation(value = "Cria uma nova pauta")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Pauta criada"),
            @ApiResponse(code = 403, message = "Violação de regra de negócio"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @PostMapping("/criar-pauta")
    @ResponseStatus(HttpStatus.CREATED)
    public void criarPauta() {
        pautaService.criarPauta();
    }

    @ApiOperation(value = "Abre a sessão da pauta com duração padrão de 60 segundos")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Sessão da pauta aberta"),
            @ApiResponse(code = 404, message = "Registro não localizado"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @PostMapping("/abrir-sessao/{pautaId}")
    @ResponseStatus(HttpStatus.OK)
    public void abrirSessao(@PathVariable Long pautaId) throws NotFoundException {
        pautaService.abrirSessao(pautaId, Sessao.DURACAO_PADRAO);
    }

    @ApiOperation(value = "Abre a sessão da pauta com duração definida pelo usuário")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Sessão da pauta aberta"),
            @ApiResponse(code = 404, message = "Registro não localizado"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @PostMapping("/abrir-sessao/{pautaId}/duracao/{duracao}")
    @ResponseStatus(HttpStatus.OK)
    public void abrirSessao(@PathVariable Long pautaId, @PathVariable Long duracao) throws NotFoundException {
        pautaService.abrirSessao(pautaId, duracao);
    }

    @ApiOperation(value = "Encerra a sessão da pauta")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Sessão da pauta encerrada"),
            @ApiResponse(code = 404, message = "Registro não localizado"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @PostMapping("/encerrar-sessao/{pautaId}")
    @ResponseStatus(HttpStatus.OK)
    public void encerrarSessao(@PathVariable Long pautaId) throws NotFoundException {
        pautaService.encerrarSessao(pautaId);
    }

    @ApiOperation(value = "Inclui voto do associado na pauta")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Voto do associado incluído na pauta"),
            @ApiResponse(code = 403, message = "Violação de regra de negócio"),
            @ApiResponse(code = 404, message = "Registro não localizado"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @PostMapping("/incluir-voto")
    @ResponseStatus(HttpStatus.OK)
    public void incluirVoto(@Valid @RequestBody VotoAssociadoDTO votoAssociado)
            throws NotFoundException, BusinessConstraintException {
        pautaService.incluirVoto(votoAssociado);
    }

    @ApiOperation(value = "Contabiliza o total de votos da pauta")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Contabilização de votos da pauta"),
            @ApiResponse(code = 404, message = "Registro não localizado"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @GetMapping("/total-votos/{pautaId}")
    public ResponseEntity<PautaVotacaoDTO> totalVotos(@PathVariable Long pautaId) throws NotFoundException {
        PautaVotacaoDTO pautaVotacao = pautaService.totalVotos(pautaId);
        return ResponseEntity.ok(pautaVotacao);
    }

}
