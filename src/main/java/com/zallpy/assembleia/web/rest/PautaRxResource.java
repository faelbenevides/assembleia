package com.zallpy.assembleia.web.rest;

import com.zallpy.assembleia.service.PautaService;
import com.zallpy.assembleia.service.dto.PautaDTO;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

/**
 * Recurso para pauta usando processos não blocante.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Api(value = "Recurso da pauta - processos não blocante")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pauta-rx")
public class PautaRxResource {

    private final PautaService pautaService;

    @ApiOperation(value = "Obtem todos os registros da pauta - processo não blocante")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Retorna a lista de pautas"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @GetMapping
    public Flux<PautaDTO> obterTodosRx() {
        return pautaService.obterTodosRx();
    }

    @ApiOperation(value = "Cria uma nova pauta - processo não blocante")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Pauta criada"),
            @ApiResponse(code = 403, message = "Violação de regra de negócio"),
            @ApiResponse(code = 409, message = "Ocorreu alguma operação ilegal"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção do servidor"), })
    @PostMapping("/criar-pauta")
    @ResponseStatus(HttpStatus.CREATED)
    public void criarPautaRx() {
        pautaService.criarPautaRx();
    }

}
