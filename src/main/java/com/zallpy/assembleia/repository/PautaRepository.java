package com.zallpy.assembleia.repository;

import java.util.List;

import com.zallpy.assembleia.domain.Pauta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repositório da entidade pauta.
 * 
 * @author Rafael Benevides
 * @since 0.0.1
 */
@Repository
public interface PautaRepository extends JpaRepository<Pauta, Long> {

    @Query("SELECT pauta FROM Pauta pauta WHERE pauta.sessao.situacao = 'ABERTA'")
    List<Pauta> findBySessaoAberta();
    
}
