package com.zallpy.assembleia;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.zallpy.assembleia");

        noClasses()
            .that()
            .resideInAnyPackage("com.zallpy.assembleia.service..")
            .or()
            .resideInAnyPackage("com.zallpy.assembleia.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.zallpy.assembleia.web..")
            .because("Serviços e repositórios não devem depender da camada da web")
            .check(importedClasses);
    }
}
