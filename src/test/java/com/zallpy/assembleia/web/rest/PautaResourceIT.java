package com.zallpy.assembleia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.zallpy.assembleia.IntegrationTest;
import com.zallpy.assembleia.client.CpfServiceClient;
import com.zallpy.assembleia.domain.Pauta;
import com.zallpy.assembleia.repository.PautaRepository;
import com.zallpy.assembleia.service.PautaService;
import com.zallpy.assembleia.service.dto.PautaVotacaoDTO;
import com.zallpy.assembleia.service.mapper.PautaMapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import reactor.core.scheduler.Scheduler;

/**
 * Testes de integração para o {@link PautaResource} REST controller.
 * 
 * @author Rafael Benevides
 */
@IntegrationTest
@AutoConfigureMockMvc
class PautaResourceIT {

    private static final String ENTITY_API_URL = "/api/pauta";

    private static boolean started = false;
    private static KafkaContainer kafkaContainer;

    @Autowired
    private PautaRepository pautaRepository;

    @Autowired
    private PautaMapper pautaMapper;

    @Autowired
    private CpfServiceClient cpfServiceClient;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private KafkaTemplate<String, PautaVotacaoDTO> kafkaTemplate;

    @Autowired
    @Qualifier("jdbcScheduler")
    private Scheduler jdbcScheduler;

    @Autowired
    private MockMvc restPautaMockMvc;

    @BeforeAll
    static void startServer() {
        if (!started) {
            startTestcontainer();
            started = true;
        }
    }

    private static void startTestcontainer() {
        /**
         * TODO: withNetwork will need to be removed soon
         * 
         * See discussion at
         * https://github.com/jhipster/generator-jhipster/issues/11544#issuecomment-609065206
         */
        DockerImageName dockerImageName = DockerImageName.parse("confluentinc/cp-kafka").withTag("5.5.3");
        kafkaContainer = new KafkaContainer(dockerImageName).withNetwork(null).withExtraHost("localhost", "127.0.0.1");
        kafkaContainer.start();
    }

    @BeforeEach
    void setup() {
        PautaService pautaService = new PautaService(pautaRepository, pautaMapper, cpfServiceClient,
                transactionTemplate, kafkaTemplate, jdbcScheduler);
        PautaResource pautaResource = new PautaResource(pautaService);

        restPautaMockMvc = MockMvcBuilders.standaloneSetup(pautaResource).build();
    }

    @Test
    @Transactional
    void criarPauta() throws Exception {
        int databaseSizeBeforeCreate = pautaRepository.findAll().size();
        // Cria a Pauta
        restPautaMockMvc.perform(post(ENTITY_API_URL + "/criar-pauta").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // Validate the Pauta in the database
        List<Pauta> pautaList = pautaRepository.findAll();
        assertThat(pautaList).hasSize(databaseSizeBeforeCreate + 1);
    }

}
