import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class RestSimulation extends Simulation {

    val httpProtocol = http
        // Here is the root for all relative URLs
        .baseUrl("http://localhost:8081")
        // Here are the common headers
        .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

    val scn = scenario("Scenario Incluir Voto")
        .exec(http("request").post("/api/pauta/incluir-voto").body(StringBody("""{ "pautaId": 1, "cpf": "19839091069", "favoravel": true }""")).asJson)

    setUp(scn.inject(atOnceUsers(1000))).protocols(httpProtocol)
}